# PeliculasPrueba

## NetworkBoundResource 
La clase NetworkBoundResource es el núcleo de la implementación, y usar el algoritmo solo requiere una subclase de NetworkBoundResource y anular algunos métodos, lo que generalmente requiere solo unas pocas líneas de código.
Si bien el algoritmo manejó los estados de red de carga, éxito y error, su caso de uso previsto es solo para dispositivos en modo en línea. 



## Lo forma en que trabajaria seria:
Si el dispositivo está fuera de línea, los valores almacenados en caché deben enviarse de inmediato.
De lo contrario, los valores almacenados en caché deben enviarse como parte de un estado de carga temporal.
Luego, el algoritmo determina si los valores almacenados en caché son lo suficientemente recientes o si los datos deben obtenerse de la red. Si es suficientemente reciente, se envían los valores almacenados en caché.

Una vez que haya implementado completamente estos métodos(sOnline, shouldLogin, autoReAuthenticate y loadFromNetwork,), cada subclase NetworkBoundResource requerirá muy poco código adicional, simplemente anulando loadFromDb, getDataFetchDate, shouldFetch, createCall y saveCallResult. 
