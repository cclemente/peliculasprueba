package com.example.piculasprueba

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.piculasprueba.model.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import android.webkit.WebChromeClient

import android.webkit.WebSettings.PluginState
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.piculasprueba.adapter.AdapterVideo


class DetalleActivity: AppCompatActivity(){
    private lateinit var binding: DetalleActivity
    private var BASE_URL ="https://api.themoviedb.org/3/movie/"
    var mediaController: MediaController?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 3
        setContentView(R.layout.fragment_second)
        val extras = intent.extras
        if (extras != null) {
            val posterPath = extras.getString("poster_path")
            val overview = extras.getString("overview")
            val releaseDate = extras.getString("release_date")
            val originalTitle = extras.getString("original_title")
            val originalLanguage = extras.getString("original_language")
            val popularity = extras.getFloat("popularity")
            val video = extras.getBoolean("video")
            val voteAverage = extras.getLong("vote_average")
            val id = extras.getLong("id")

            val sunset: ImageView = findViewById(R.id.photoDetalle)
            sunset.fromUrl("https://image.tmdb.org/t/p/w500/" + posterPath)
            val originalT: TextView = findViewById(R.id.originalTitle)
            originalT.text = Html.fromHtml("<b>Título: </b>$originalTitle")
            val release: TextView = findViewById(R.id.release_date)
            release.text = Html.fromHtml("<b>Año: </b>$releaseDate")
            val originalL: TextView = findViewById(R.id.original_language)
            originalL.text = Html.fromHtml("<b>Idioma: </b>$originalLanguage")
            val popular: TextView = findViewById(R.id.popularity)
            popular.text = Html.fromHtml("<b>Puntuación: </b>$popularity")
            val over: TextView = findViewById(R.id.overview)
            over.text = Html.fromHtml("<b>Información general \n: </b>$overview")
            searchVideo("$id/videos")
            val scrollI: ImageView = findViewById(R.id.myIcon)
            //The key argument here must match that used in the other activity
        // searchVideo("$id/videos")
        }

        //  val navController = findNavController(R.id.nav_host_fragment_content_main)
        //  appBarConfiguration = AppBarConfiguration(navController.graph)
        //  setupActionBarWithNavController(navController, appBarConfiguration)

    }
    private fun initCharacter(puppies: MutableList<VideosModel>) {
    /*
        var mWebView: WebView =findViewById(R.id.videoWeb)
        mWebView.settings.javaScriptEnabled=true
       // mWebView.settings.pluginState(PluginState.ON)
        mWebView.settings.pluginState=PluginState.ON
        mWebView.webViewClient= WebViewClient()
        mWebView.loadUrl("http://www.youtube.com/embed/" + puppies[0].key + "?autoplay=1&vq=small")
    */

    // GGe_h2MWMrs
        //mWebView.webChromeClient=WebChromeClient()

/*
        var videoDetalle: VideoView =findViewById(R.id.videoV)
        if (mediaController==null){
            mediaController= MediaController(this)
            mediaController!!.setAnchorView(videoDetalle)
        }
        videoDetalle!!.setMediaController(mediaController)
       // videoDetalle.setVideoURI(Uri.parse("https://www.youtube.com/watch?v=$puppies[0].key"))

        videoDetalle.setVideoPath("https://videocdn.bodybuilding.com/video/mp4/62000/62792m.mp4")
        videoDetalle.requestFocus();
        videoDetalle.start()
        videoDetalle.setOnCompletionListener {
            Toast.makeText(applicationContext,"Fin video",Toast.LENGTH_LONG).show()
        }
        videoDetalle!!.setOnErrorListener() {mediaPlayer, i, i2 ->
            Toast.makeText(applicationContext,"Fin video",Toast.LENGTH_LONG).show()
            false
        }
        Log.e("UriVideo","https://www.youtube.com/watch?v="+puppies[0].key)
*/
        var dogsAdapter = AdapterVideo(puppies);
        var recyclerView: RecyclerView = findViewById(R.id.rvVideo)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager= LinearLayoutManager(this@DetalleActivity)
        (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(2)
        dogsAdapter.also { recyclerView.adapter = it }

    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    private fun searchVideo(query: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val call = getRetrofit().create(APIServiceVideo::class.java).getDogsByBreeds("$query?api_key=06079216f8246da9db2572d396c1a149")
            val puppies = call.body()
            runOnUiThread {
                Log.e("url",BASE_URL+"$query?api_key=06079216f8246da9db2572d396c1a149")
                puppies?.let { initCharacter(it.results) }
               // binding.rvDogs.adapter?.notifyDataSetChanged()
            }
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}