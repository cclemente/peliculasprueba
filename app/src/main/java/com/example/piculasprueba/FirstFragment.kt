package com.example.piculasprueba

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import com.example.piculasprueba.adapter.AdapterMovie
import com.example.piculasprueba.databinding.FragmentFirstBinding
import com.example.piculasprueba.model.APIService
import com.example.piculasprueba.model.Results
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.support.v4.runOnUiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {
    private var BASE_URL ="https://api.themoviedb.org/3/movie/"
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        searchByName("now_playing")
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }
    private fun searchByName(query: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val call = getRetrofit().create(APIService::class.java).getDogsByBreeds("$query?api_key=06079216f8246da9db2572d396c1a149")
            val puppies = call.body()
            runOnUiThread {
                Log.e("url",BASE_URL+"$query?api_key=06079216f8246da9db2572d396c1a149")
                puppies?.let { initCharacter(it.results) }
                binding.rvDogs.adapter?.notifyDataSetChanged()
            }
        }
    }
    private fun initCharacter(puppies: MutableList<Results>){
        var dogsAdapter = AdapterMovie(puppies);
        binding.rvDogs.setHasFixedSize(true)
        binding.rvDogs.layoutManager =  GridLayoutManager(context, 2)
        binding.rvDogs.adapter = dogsAdapter
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val spinner = view.findViewById<Spinner>(R.id.spinner2)
        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                println("erreur")
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val type = parent?.getItemAtPosition(position).toString()
                Toast.makeText(activity,type, Toast.LENGTH_LONG).show()
                println(type)
                if (type=="Playing Now"){
                    searchByName("now_playing")
                }
                if (type=="Most Popular"){
                    searchByName("popular")
                }
                Log.e("holaa",type)
            }
        }
      //  binding.buttonFirst.setOnClickListener {
     //       findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
      //  }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}