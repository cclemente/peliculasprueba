package com.example.piculasprueba

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.recyclerview.widget.GridLayoutManager
import com.example.piculasprueba.adapter.AdapterMovie
import com.example.piculasprueba.databinding.ActivityMainBinding
import com.example.piculasprueba.model.APIService
import com.example.piculasprueba.model.Results
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private var BASE_URL ="https://api.themoviedb.org/3/movie/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
       // setSupportActionBar(binding.toolbar)
        val spinner: Spinner = this.findViewById(R.id.spinner2)
        spinner.onItemSelectedListener = this

      //  val navController = findNavController(R.id.nav_host_fragment_content_main)
      //  appBarConfiguration = AppBarConfiguration(navController.graph)
      //  setupActionBarWithNavController(navController, appBarConfiguration)
        searchByName("now_playing")

    }
    private fun searchByName(query: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val call = getRetrofit().create(APIService::class.java).getDogsByBreeds("$query?api_key=06079216f8246da9db2572d396c1a149")
            val puppies = call.body()
            runOnUiThread {
                Log.e("url",BASE_URL+"$query?api_key=06079216f8246da9db2572d396c1a149")
                puppies?.let { initCharacter(it.results) }
                binding.rvDogs.adapter?.notifyDataSetChanged()
            }
        }
    }
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val type = parent?.getItemAtPosition(position).toString()
        println(type)
        if (type=="Playing Now"){
            searchByName("now_playing")
        }
        if (type=="Most Popular"){
            searchByName("popular")
        }
        Log.e("holaa",type)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    private fun initCharacter(puppies: MutableList<Results>){
        var dogsAdapter = AdapterMovie(puppies);
        binding.rvDogs.setHasFixedSize(true)
        binding.rvDogs.layoutManager =  GridLayoutManager(this@MainActivity, 2)
        binding.rvDogs.adapter = dogsAdapter
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.overflow_menu, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}