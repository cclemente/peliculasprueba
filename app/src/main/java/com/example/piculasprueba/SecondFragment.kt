package com.example.piculasprueba

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.example.piculasprueba.databinding.FragmentSecondBinding
import com.example.piculasprueba.model.OnTextClickListener

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */


class SecondFragment : Fragment(),OnTextClickListener{
    private var _binding: FragmentSecondBinding? = null
    private val myArgs: SecondFragmentArgs by navArgs()
    private val binding get() = _binding!!
    var myValue = this.arguments?.getString("myArg")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fun sendData(valor: String) {
            Toast.makeText(context, "El valor recibido es: " + valor, Toast.LENGTH_SHORT).show()
        }
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root

    }
    override fun sendData(data: String) {
        Toast.makeText(context, "El valor recibido es: " + data, Toast.LENGTH_SHORT).show()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e("text",myValue.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}