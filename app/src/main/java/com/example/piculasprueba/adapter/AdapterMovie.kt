package com.example.piculasprueba.adapter

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import com.example.piculasprueba.databinding.ItemmovieBinding
import android.content.Intent
import com.example.piculasprueba.DetalleActivity
import com.example.piculasprueba.R
import com.example.piculasprueba.model.OnTextClickListener
import com.example.piculasprueba.model.Results
import com.example.piculasprueba.model.fromUrl
import org.jetbrains.anko.sdk27.coroutines.onClick


class AdapterMovie(val images: MutableList<Results>) : RecyclerView.Adapter<AdapterMovie.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = images[position]
        Log.e("HolaURL", "https://image.tmdb.org/t/p/w500/$item")
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.itemmovie, parent, false))
    }

    override fun getItemCount(): Int {
        return images.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemmovieBinding.bind(view)
        var navController: NavController? = null
        var listener: OnTextClickListener? = null

        fun bind(image: Results) {
            Log.e("HolaURL", "https://image.tmdb.org/t/p/w500/$image.poster_path")
            binding.ivDog.fromUrl("https://image.tmdb.org/t/p/w500/"+image.poster_path)
            binding.ivDog.onClick {
                Log.e("onClick", "https://image.tmdb.org/t/p/w500/$image.poster_path")
                val intent = Intent(itemView.context, DetalleActivity::class.java)
                intent.putExtra("id",image.id);
                intent.putExtra("poster_path",image.poster_path);
                intent.putExtra("overview",image.overview);
                intent.putExtra("release_date",image.release_date);
                intent.putExtra("original_title",image.original_title);
                intent.putExtra("original_language",image.original_language);
                intent.putExtra("popularity",image.popularity);
                intent.putExtra("video",image.video);
                intent.putExtra("vote_average",image.vote_average);

                itemView.context.startActivity(intent)
            }
        }
    }
}