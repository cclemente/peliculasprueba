package com.example.piculasprueba.adapter

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import com.example.piculasprueba.databinding.ItemvideoBinding
import android.content.Intent
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import com.example.piculasprueba.DetalleActivity
import com.example.piculasprueba.R
import com.example.piculasprueba.model.OnTextClickListener
import com.example.piculasprueba.model.Results
import com.example.piculasprueba.model.VideosModel


class AdapterVideo(val images: MutableList<VideosModel>) : RecyclerView.Adapter<AdapterVideo.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = images[position]
        Log.e("HolaURL", "https://image.tmdb.org/t/p/w500/$item")
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.itemvideo, parent, false))
    }

    override fun getItemCount(): Int {
        return images.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemvideoBinding.bind(view)
        fun bind(image: VideosModel) {
            Log.e("HolaURL", "https://image.tmdb.org/t/p/w500/$image.poster_path")
            binding.videoWeb.settings.javaScriptEnabled=true
            // mWebView.settings.pluginState(PluginState.ON)
            binding.videoWeb.settings.pluginState= WebSettings.PluginState.ON
            binding.videoWeb.webViewClient= WebViewClient()
            binding.videoWeb.loadUrl("http://www.youtube.com/embed/" + image.key + "?autoplay=1&vq=small")
        }
    }
}