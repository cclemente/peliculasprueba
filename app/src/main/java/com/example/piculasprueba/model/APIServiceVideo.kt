package com.example.piculasprueba.model

import retrofit2.Response
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url
interface APIServiceVideo {
    @GET
    suspend fun getDogsByBreeds(@Url url:String): Response<JsonVideo>
}