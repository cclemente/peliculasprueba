package com.example.piculasprueba.model
import com.google.gson.annotations.SerializedName

data class JsonMovie (
    @SerializedName("page") var page: Int,
    @SerializedName("results") var results: MutableList<Results>,
    @SerializedName("total_results") var total_results:Int,
    @SerializedName("total_pages") var total_pages:Int
)