package com.example.piculasprueba.model
import com.google.gson.annotations.SerializedName

data class JsonVideo (
    @SerializedName("id") var id: Int,
    @SerializedName("results") var results: MutableList<VideosModel>,
)