package com.example.piculasprueba.model

import com.example.piculasprueba.model.Movie

data class Movie(
    val movie_id: Int,
    val title: String?,
    val poster: String?,
    val backdrop: String?,
    val overview: String?,
    val rating: String?,
    val release_date: String?,
    var favorite: Int = 0,
    var trailer: String?,
    val page: Int
)