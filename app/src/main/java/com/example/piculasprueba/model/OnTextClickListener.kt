package com.example.piculasprueba.model

public interface OnTextClickListener {
    fun sendData(data: String)
}
