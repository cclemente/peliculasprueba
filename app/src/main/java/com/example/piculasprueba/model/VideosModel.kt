package com.example.piculasprueba.model
import com.google.gson.annotations.SerializedName

data class VideosModel(
    @SerializedName("iso_639_1") val iso_639_1: String,
    @SerializedName("iso_3166_1") val iso_3166_1: String,
    @SerializedName("name") val name: String,
    @SerializedName("key") val key: String,
    @SerializedName("site") var site: String,
    @SerializedName("size") val size:Int,
    @SerializedName("type") val type:String,
    @SerializedName("official") val official:Boolean,
    @SerializedName("published_at") val published_at: String,
    @SerializedName("id") val id: String
)